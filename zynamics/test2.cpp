#include <iostream>
#include <cstdio>
#include <string.h>

using namespace std;

int fib(int n){
	
	int fibminus2=0;
	int fibminus1=1;
	int fib=0;
	int i;

	if (n==0 || n==1) 
		return n;
	for(i=2;i<=n;i++){
		fib=fibminus1+fibminus2;
		fibminus2=fibminus1;
		fibminus1=fib;
	}
	return fib;
}


int main() {
	int a[10] = {6, 1, 4, 12, 5645, 43, 6, 7, 7, 34};
	//cout << "Write 10 numbers" << endl;
	//for(int i = 0; i < 10; i++){
	//	cin >> a[i];
	//}
	
	for(int i = 0; i < 10; i++){
		fib(a[i]);
	}
	return 0;
}
