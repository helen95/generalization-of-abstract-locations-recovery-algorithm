#include <iostream>
#include <cstdio>
#include <string.h>

using namespace std;

void remove_dublicates(char* str) {

	if (str == NULL) return;
	int len = strlen(str);
	if (len < 2) return;

	int tail = 1;
	for (int i = 1; i < len; i++) {
		int j = 0;
		for (j = 0; j < tail; j++) {

			if (str[i] == str[j]) break;
		}

		if (j == tail) {

			tail++;
			str[tail - 1] = str[i];
		}
	}
	str[tail] = 0;
}

int main() {
	char str[15];// = "aaaabbbccd\0";
	cin >> str;
	remove_dublicates(str);

	cout << str << endl;
	return 0;
}
