# -*- coding: utf-8 -*-
from library import DEBUG

from ASI_TypeAnalysis import Abs
from ASI_TypeAnalysis import LIST_OF_ATOM_ID
from ASI_TypeAnalysis import FindValOfEqClass

from AtomicTreeClass import DICT_OF_ATOM_TREES # dictionary key='name', elem=AtomicTree()
from AtomicTreeClass import Atom 
from AtomicTreeClass import SumOfTrees
from AtomicTreeClass import TimesInTree

from DataRefClass import ProgVar
from DataRefClass import IntervalOfDataRef
from DataRefClass import ArrayOfDataRef

from AtomicRefsClass import AtomicReference

from EquivalentClass import Atomic
from EquivalentClass import SumOfElems
from EquivalentClass import TimesInArray

from PrintTreeClass import PrintTree
from PrintTreeClass import PrintTreeList
from PrintTreeClass import PrintTreeDict

##################################### ATOMIZATION #############################

def AbsForTrees( x ):           # length of tree x
    """
    x: AtomicTreeClass()        # tree
    ret: int                    # return size of tree
    """
    if ( isinstance( x, Atom ) ) :
        
        return x.size
        
    else:
        if( isinstance( x, SumOfTrees ) ) :
            
            return AbsForTrees( x.atomicTreeL ) + AbsForTrees( x.atomicTreeR )
            
        else:
            if( isinstance( x, TimesInTree ) ) :
                
                return x.count * AbsForTrees( x.atomicTree )
        
                
def GenSymbols( atomID = None ): 
    """
    atomID: 'string'            # old name of variable ( if it existed )
    ret: 'string'               # new name for variable
    """    
    if ( atomID in LIST_OF_ATOM_ID ) or ( atomID == None ):
        
        new_name = 'a_%d' %( len( LIST_OF_ATOM_ID ) )
        LIST_OF_ATOM_ID.append( new_name )
        return new_name
        
    else: #### Delete this part ??????????
        
        LIST_OF_ATOM_ID.append( atomID )
        return atomID
        

def SetAtomicTree( prgVar, atomicTree ):
    """
    prgVar: 'string'            # name of tree
    atomicTree: AtomicTree()    # tree, which name will be prgVar
    """
    atomicTree.treeName = prgVar
    DICT_OF_ATOM_TREES.update( [ ( prgVar, atomicTree ) ] )

    
def GetAtomicTree( prgVar ):
    """
    prgVar: 'string'            # name of tree
    ret: AtomicTree()           # get tree with name = prgVar
    """
    return DICT_OF_ATOM_TREES.get( prgVar )
    
    
def TermVarOf( prgVar, sz ): 
    """
    prgVar: 'string'            # name of tree
    sz: int                     # size of new node
    ret: EqClass()              # return new node of new tree with name = prgVar
    """    
    return Atomic( sz, prgVar ) 



def Flatten( termVar, multiplicity ):
    """
    termVar: EqClass()          # term variable 
    multiplicity: int           # multiplicity 
    ret: AtomicTree()           # creating tree by term variable
    """
    newTermVar = FindValOfEqClass( termVar )
    if ( isinstance( newTermVar, Atomic ) ) : #findVal??
        
        return Atom( GenSymbols( newTermVar.nodeName ), newTermVar.size, multiplicity )
        
    elif ( isinstance( newTermVar, SumOfElems ) ) :
        
        return SumOfTrees( Flatten( newTermVar.varL, multiplicity ), \
                           Flatten( newTermVar.varR, multiplicity ) )
              
    elif ( isinstance( newTermVar, TimesInArray ) ) :
        
        count = newTermVar.count
        return TimesInTree( count, Flatten( newTermVar.elem, multiplicity * count ) )
        

def DoRightAssociate( term ):
    """
    term: AtomicTree()          # tree
    ret: AtomicTree()           # new right associate tree
    """
    if isinstance( term, SumOfTrees ):
        
        leftTree = term.atomicTreeL
        rightTree = term.atomicTreeR
        
        if isinstance( leftTree, SumOfTrees ):
                     
            newLeftTree = leftTree.atomicTreeL
            newLeftOfRightTree = leftTree.atomicTreeR
            return SumOfTrees( DoRightAssociate( newLeftTree ), \
                    DoRightAssociate( SumOfTrees( newLeftOfRightTree, rightTree ) ) )
            
        return SumOfTrees( DoRightAssociate( leftTree ), DoRightAssociate( rightTree ) )
    
    if isinstance( term, TimesInTree ):
        
        return TimesInTree( term.count, DoRightAssociate( term.atomicTree ) )
        
    if isinstance( term, Atom ):
        
        return term



def AtomicTreeOf( prgVar ):
    """
    prgVar: DataRef()           # data reference
    ret: AtomicTree()           # get new ( keeped in dictionary ) tree for prgVar
    """
    atomicTree = GetAtomicTree( prgVar.varName )
    
    if atomicTree != None:
        
        return atomicTree
    
    else:
        
        SetAtomicTree( prgVar.varName, DoRightAssociate( \
                        Flatten( TermVarOf( prgVar.varName, prgVar.sz ), 1 ) ) )
        #PrintTree(AtomicTreeOf( prgVar ))
        return AtomicTreeOf( prgVar )

        

def Head( tree, i ):
    """
    tree: AtomicTree()          # tree
    i: int                      # index of end
    ret: AtomicTree()           # subtree from 1 to i
    """
    if isinstance( tree, SumOfTrees ):
        
        if i > AbsForTrees( tree.atomicTreeL ):
            
            return SumOfTrees( tree.atomicTreeL, Head( tree.atomicTreeR,\
                                    i - AbsForTrees( tree.atomicTreeL ) ), tree.treeName )
            
        else:
            """
            if i < AbsForTrees( tree.atomicTreeL ):
                
                subTree = tree.atomicTreeL
                if isinstance( subTree, TimesInTree ):
                    
                    countElems = i / AbsForTrees( subTree )
                    if countElems <= 0x01:
                        
                        return Head( subTree.atomicTree, i )
                    
                    else:
                    
                        return TimesInTree( countElems, subTree.atomicTree )
                """
            return tree.atomicTreeL
            
    return tree


def Tail( tree, i ):
    """
    tree: AtomicTree()          # tree
    i: int                      # index of start
    ret: AtomicTree()           # subtree from i to end
    """
    if i == 1:
        
        return tree
      
    if isinstance( tree, SumOfTrees ):
        
        return Tail( tree.atomicTreeR, i - AbsForTrees( tree.atomicTreeL ) )
        
def Subrange( tree, i, j ):
    """
    tree: AtomicTree()          # tree
    i: int                      # index of start
    j: int                      # index of end
    ret: AtomicTree()           # subtree from i to j
    """
    return Head( Tail( tree, i ), j - i + 1 )
       
    
def BreakUp( tree, s ):
    """
    tree: AtomicTree()          # tree
    s: int                      # size of one array's element 
    ret: list( AtomicTree() )   # list of useful(splited) subtrees
    """
        
    if AbsForTrees( tree ) == s:
        
        return [ tree ]
    
    if isinstance( tree, SumOfTrees ):
        
        leftTree = tree.atomicTreeL
        rightTree = tree.atomicTreeR
        
        if AbsForTrees( leftTree ) > s:
            
            return BreakUp( leftTree, s ) + BreakUp( rightTree, s ) 
            
        else:
            
            return [ Head( tree, s ) ] + BreakUp( Tail( tree, s + 1 ), s )
            
    if isinstance( tree, TimesInTree ):
    
        return BreakUp( tree.atomicTree, s )

        
def DoIntervalSplit( tree, start, end, startOfAtomInTree = 1 ):
    """
    tree: AtomicTree()          # tree
    start: int                  # start of interval
    end: int                    # end of interval
    ret: AtomicTree()           # return splited subtree 
    """
    if ( ( 0x01 == start ) and ( end == AbsForTrees( tree ) ) ) or ( end - start + 1 == AbsForTrees( tree ) ):
        
        return tree
        
    elif isinstance( tree, Atom ): 
        
        if 0x01 == start:
                
            newTree = SumOfTrees( Atom( GenSymbols(), end, 1 ), \
                                   Atom( GenSymbols(), tree.size - end, 1 ) )
        
        elif end == tree.size:
            
            newTree = SumOfTrees( Atom( GenSymbols(), start - 1, 1 ), \
                                   Atom( GenSymbols(), end - start + 1, 1 ) )
            
        else:
            
            rightSubTree = SumOfTrees( Atom( GenSymbols(), end - start + 1, 1 ), \
                                    Atom( GenSymbols(), tree.size - end, 1 ) )
            SetAtomicTree( GenSymbols(), rightSubTree )
            newTree = SumOfTrees( Atom( GenSymbols(), start - 1, 1 ), rightSubTree )
    
    elif isinstance( tree, SumOfTrees ):
        
        leftSubTree = tree.atomicTreeL
        rightSubTree = tree.atomicTreeR
        
        if AbsForTrees( leftSubTree ) > start - 1:
            
            if AbsForTrees( leftSubTree ) >= end:
                
                newTree = SumOfTrees( DoIntervalSplit( leftSubTree, start, end ),\
                                     rightSubTree )
             
            else:
                
                newLeftSubTree = DoIntervalSplit( leftSubTree, start, \
                                              AbsForTrees( leftSubTree ) )
                newRightSubTree = DoIntervalSplit( rightSubTree, 1, \
                                        end - AbsForTrees( leftSubTree ) )
                newTree = SumOfTrees( newLeftSubTree, newRightSubTree )
            
        else:
            
            newTree = SumOfTrees( leftSubTree, DoIntervalSplit( rightSubTree, \
                                    start - AbsForTrees( leftSubTree ), \
                                    end - AbsForTrees( leftSubTree ) ) )
        
        
    elif isinstance( tree, TimesInTree ):
        
        arrayElem = tree.atomicTree
        elemSz = AbsForTrees( tree ) / tree.count

        if end - start + 1 > elemSz:
            
            if DEBUG:
                print "ERROR: try to use interval, which size larger then size of array element"
                
            return tree
            
        newAtomicTree = DoIntervalSplit( arrayElem, start, end, start ) 
        
        newTree = TimesInTree( tree.count, newAtomicTree )
        
       
    SetAtomicTree( tree.treeName, DoRightAssociate( newTree ) )
    return GetAtomicTree( tree.treeName )
        

def DoArraySplit( tree, countElem, arrayElemName ):
    """
    tree: AtomicTree()          # tree
    countElem: int              # count of nodes, that supposed to be after spliting
    arrayElemName: 'string'     # name of node
    ret: AtomicTree()           # return splited subtree
    """
        
    if countElem <= 1:
        
        if DEBUG:
            if countElem == 0:
                print "ERROR: You trying to split array on 0 element "
                
        return tree
    
    elemSz = AbsForTrees( tree ) / countElem    
    
    if isinstance( tree, Atom ):
        
        arrayElem = GetAtomicTree( arrayElemName )
        
        if arrayElem:
            
            newTree = TimesInTree( countElem, arrayElem)
        
        else:
        
            newTree = TimesInTree( countElem, Atom( arrayElemName, elemSz, \
                                                countElem * tree.multiplicity ) )
        
    elif isinstance( tree, SumOfTrees ):
        
        leftCountElem = AbsForTrees( tree.atomicTreeL ) / elemSz
        rightCountElem = AbsForTrees( tree.atomicTreeR ) / elemSz

        leftSubTree = DoArraySplit( tree.atomicTreeL, leftCountElem, arrayElemName )        
        rightSubTree = DoArraySplit( tree.atomicTreeR, rightCountElem, arrayElemName )
        newTree = SumOfTrees( leftSubTree, rightSubTree )
        
    elif isinstance( tree, TimesInTree ): 
        
        newTree = tree
        
    SetAtomicTree( tree.treeName, DoRightAssociate( newTree ) )
    return GetAtomicTree( tree.treeName )
   
     
def TreesOf( x ): 
    """
    x: DataRef()                # left (a) or right (b) part of constraint, a <- b
    ret: list( AtomicTree() )   # list of useful(part of constraints) subtrees
    """
    if isinstance( x, ProgVar ):
        
        return [ AtomicTreeOf( x ) ]
        
    if isinstance( x, IntervalOfDataRef ):
        
        res_list = []
        
        for t in TreesOf( x.dataRef ):
            
            t = DoIntervalSplit( t, x.low, x.up )
            res_list.append( Subrange( t, x.low, x.up ) ) # Maybe BreakUp(t) ???

        return res_list
                 
    if isinstance( x, ArrayOfDataRef ):
        
        treesOf = TreesOf( x.dataRef )  

        res_list = []
        
        for t in treesOf:
            
            t = DoArraySplit( t, x.countOfElem, GenSymbols() )
            res_list += BreakUp( t, AbsForTrees( t ) / x.countOfElem )
        
        return list( set( res_list ) )
        
            
def Leaves( tree, m ):
    """
    tree: AtomicTree()              # tree
    m: int                          # multiplicity of nodes
    ret: list( AtomicReference() )  # return list of leaves, which used in constraints
    """
    if isinstance( tree, Atom ):
        
        newEqClass = TermVarOf( tree.atomID, AbsForTrees( tree ) ) # tree.treeName???
        newEqClass.multiplicity = tree.multiplicity
        return [ AtomicReference( newEqClass, tree.multiplicity) ] 
                
    if isinstance( tree, SumOfTrees ):
        
        return Leaves( tree.atomicTreeL, m ) + Leaves( tree.atomicTreeR, m )
        
    if isinstance( tree, TimesInTree ):
        
        return Leaves( tree.atomicTree, m * tree.count )

        
 
def D1( d ): 
    """
    d: DataRef()                      # data reference (left or right part of constraint)
    ret: list( AtomicReference() )    # return list of leaves(used in constraints) of builded ASI tree
    """
    listOfAtomRefs = []
   
    for t in TreesOf( d ):
        
        listOfAtomRefs += Leaves( t, 1 )
        
    return listOfAtomRefs