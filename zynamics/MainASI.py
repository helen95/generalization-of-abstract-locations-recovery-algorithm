# -*- coding: utf-8 -*-

from ASI_TypeAnalysis import SolveConstraint

from ASI_Atomization import Leaves
from ASI_Atomization import D1

from DataRefClass import ProgVar
from DataRefClass import IntervalOfDataRef
from DataRefClass import ArrayOfDataRef

from PrintEqClass import PrintEqClass
from PrintEqClass import PrintEqClassList
from PrintAtomicRefClass import PrintAtomicRefList

from ConstraintsClass import EQUIVALENT
from AtomicTreeClass import DICT_OF_ATOM_TREES
#from library import DEBUG

EXAMLE_NUM = 4

class Term:
    """
    leftDataRef: DataRef() or list( EqClass() )
    rightDataRef: DataRef() or list( EqClass() )
    constraint: EQUIVALENT
    """
    
    def __init__( self, leftDataRef, rightDataRef, constraint = EQUIVALENT ):
        
        self.leftDataRef = leftDataRef
        self.rightDataRef = rightDataRef
        self.constraint = constraint

        
def SolveASI( listOfTerms ):
    
    listOfAtoms = []

    for term in listOfTerms:
            
        leftTreeList = D1( term.leftDataRef )
        rightTreeList = D1( term.rightDataRef )
        splitedTermList = Term( leftTreeList, rightTreeList, term.constraint )
        #PrintAtomicRefList( leftTreeList )
        #PrintAtomicRefList( rightTreeList )
        commonElem = SolveConstraint( splitedTermList )
        listOfAtoms.append( commonElem )
    
    return listOfAtoms
		
				
def DoASI( term ):
    
	leftTreeList = D1( term.leftDataRef )
	rightTreeList = D1( term.rightDataRef )
	splitedTermList = Term( leftTreeList, rightTreeList, term.constraint )
	#PrintAtomicRefList( leftTreeList )
	#PrintAtomicRefList( rightTreeList )
	return SolveConstraint( splitedTermList )

        
#"""      
def main() :
   
    ######## Fig. 8 example ############
    # |P| = 20, |Q| = 10, |R| = 2
    # P[1:10] = Q[1:10]
    # P[1:20]\10 = R[1:2]
    if EXAMLE_NUM == 1:
        
        leftDataRef1 = IntervalOfDataRef( ProgVar( 'P', 20 ), 1, 10 )
        rightDataRef1 = IntervalOfDataRef( ProgVar( 'Q', 10 ), 1, 10 )
        term1 = Term( leftDataRef1, rightDataRef1, EQUIVALENT )
        
        termOfArray = IntervalOfDataRef( ProgVar( 'P', 20 ), 1, 20 )
        leftDataRef2 = ArrayOfDataRef( termOfArray, 10 )
        rightDataRef2 = IntervalOfDataRef( ProgVar( 'R', 2 ), 1, 2 )
        term2 = Term( leftDataRef2, rightDataRef2, EQUIVALENT )
        
        terms = [ term1, term2 ]
        listOf_ARregions = ['P', 'Q', 'R']  
        
         
    
    ######## Fig. 6 example ############
    # |X| = 12, |W| = 6, |Y| = 8, |Z| = 12
    # X[5:8] = Y[1:4]
    # Z[1:6] = W[1:6]
    # X[3:12] = Z[1:10]
    elif EXAMLE_NUM == 2:
        
        terms = []
        terms.append( Term( IntervalOfDataRef( ProgVar( 'X', 12 ), 5, 8 ),\
                      IntervalOfDataRef( ProgVar( 'Y', 8 ), 1, 4 ) ) )
        terms.append( Term( IntervalOfDataRef( ProgVar( 'Z', 12 ), 1, 6 ),\
                      IntervalOfDataRef( ProgVar( 'W', 6 ), 1, 6 ) ) )
        terms.append( Term( IntervalOfDataRef( ProgVar( 'X', 12 ), 3, 12 ),\
                      IntervalOfDataRef( ProgVar( 'Z', 12 ), 1, 10 ) ) )
        
        listOf_ARregions = ['X', 'Y', 'Z', 'W']  
        
    ######## WYSINWYX Fig. 5.3 example ############
    # |pts| = 40
    # pts[1:40]\5[1:4] = a[1:4]
    # pts[1:40]\5[5:8] = b[1:4]
    
    elif EXAMLE_NUM == 3:
        
        terms = []
        terms.append( Term( IntervalOfDataRef( ArrayOfDataRef( IntervalOfDataRef\
                                    ( ProgVar( 'pts', 40 ), 1, 40 ), 5 ), 1, 4 ),\
                                  IntervalOfDataRef( ProgVar( 'a', 4 ), 1, 4 ) ) )
        terms.append( Term( IntervalOfDataRef( ArrayOfDataRef( IntervalOfDataRef\
                                    ( ProgVar( 'pts', 40 ), 1, 40 ), 5 ), 5, 8 ),\
                                  IntervalOfDataRef( ProgVar( 'b', 4 ), 1, 4 ) ) )

        listOf_ARregions = ['pts', 'a', 'b']  


    elif EXAMLE_NUM == 4:
        
        terms = [ Term( IntervalOfDataRef( ProgVar( 'INIT_ESP', 4 ), 1, 4 ),\
                                  IntervalOfDataRef( ProgVar( 'GLOBAL', 4 ), 1, 4 ) ) ]
        listOf_ARregions = ['INIT_ESP', 'GLOBAL']
    
    listOfAtoms = SolveASI( terms )
    #PrintEqClassList( listOfAtoms )

    for arRegName in listOf_ARregions:
        
        PrintAtomicRefList( Leaves( DICT_OF_ATOM_TREES[ arRegName ], 1 ) )
            
        
    
if __name__ == '__main__':
    
	main()
 
#"""