#include <iostream>
#include <cstdio>
#include <string.h>

int main(){
	int a[10], i;
	for(i = 0; i < 10; i++){
		if (i > 1)
			a[i] = i*i + a[i - 1];
		else
			a[i] = i * i;
	}
	return 0;
}