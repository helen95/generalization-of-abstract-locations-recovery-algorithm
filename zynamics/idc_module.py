# -*- coding: utf-8 -*-

import idc
import json


def AddComments( objs ):
	
	for i in range(len(objs)):
		
		comment = "Left Op:\n" + str(objs[str(i+1)]['leftComment']) + \
			    "Right Op:\n" + str(objs[str(i+1)]['rightComment'])
		
		idc.MakeComm( objs[str(i+1)]['addressOfREIL-instr'], comment )


jsonFile = open('json.txt', 'r')
jsonObjs = json.load(jsonFile)
#print jsonObjs
AddComments( jsonObjs )

#idc.Exit(0)
