#include <stdio.h>
#include <iostream>

using namespace std;



class Point {
private:
	double x;
	double y;
public:
	Point(double xNew = 0.0, double yNew = 0.0) {
		x = xNew;
		y = yNew;
	}

	void setPoint(double newX, double newY);

	Point add(const Point otherPoint);

	Point subtract(const Point otherPoint);

	void printPoint();

};

void Point::setPoint(double newX, double newY)
{
	x = newX;
	y = newY;
}

Point Point::add(const Point p)
{
	Point z;
	z.x = x + p.x;
	z.y = y + p.y;

	return z;
}

Point Point::subtract(const Point p)
{
	Point z;
	z.x = x - p.x;
	z.y = y - p.y;

	return z;
}



void Point::printPoint()
{
	cout << "(" << x << "," << y << ")";
}

int main(void)
{
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	Point a(1, 7), b(9, 8), c;

	a.printPoint();
	cout << " + ";
	b.printPoint();
	cout << " = ";
	c = a.add(b);
	c.printPoint();

	cout << '\n';
	a.setPoint(10, 1);
	b.setPoint(11, 3);
	a.printPoint();
	cout << " - ";
	b.printPoint();
	cout << " = ";
	c = a.subtract(b);
	c.printPoint();
	cout << endl;
	
	return 0;
}