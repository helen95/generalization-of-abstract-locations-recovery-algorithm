# -*- coding: utf-8 -*-


DICT_OF_ATOM_TREES = {}

class AtomicTree:
    """
    treeName: 'string'              # name of node with subtree, rooted in this node
    """
    treeName = None
    
    

class Atom( AtomicTree ):

    def __init__( self, atomID, size, multiplicity, start = 1 ):  
        """
        atomID: 'string'            # name of atom 
        size: int                   # size of atom
        multiplicity: int           # multiplicity of atom in tree
        """
        self.atomID = atomID
        self.size = size
        self.multiplicity = multiplicity
        self.treeName = atomID
        self.start = 1
        DICT_OF_ATOM_TREES[ atomID ] = self
 
       
class SumOfTrees( AtomicTree ):
    
    def __init__( self, atomicTreeL, atomicTreeR, treeName = None ):
        """
        atomicTreeL: AtomicTree()   # left subtree
        atomicTreeR: AtomicTree()   # right subtree
        """
        self.atomicTreeL = atomicTreeL
        self.atomicTreeR = atomicTreeR
        self.treeName = treeName

        
class TimesInTree( AtomicTree ):
    
    def __init__( self, count, atomicTree ):
        """
        count: int                  # count of similar nodes in tree
        atomicTree: AtomicTree()    # one of a set of similar tree nodes
        """
        self.count = count
        self.atomicTree = atomicTree
