# -*- coding: utf-8 -*-

#from fractions import gcd

DEBUG = 0x01 # if it's a RELEASE version then put DEBUG = 0x00

def gcd(a, b):
    """Calculate the Greatest Common Divisor of a and b.

    Unless b==0, the result will have the same sign as b (so that when
    b is divided by it, the result comes out positive).
    """
    while b:
        a, b = b, a%b
    return a

# define lcm function
def lcm(x, y):
   """This function takes two
   integers and returns the L.C.M."""

   lcm = ( x * y )//gcd( x,y )
   return lcm
