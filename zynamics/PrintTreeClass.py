# -*- coding: utf-8 -*-

from AtomicTreeClass import Atom
from AtomicTreeClass import SumOfTrees
from AtomicTreeClass import TimesInTree

from AtomicTreeClass import DICT_OF_ATOM_TREES

def PrintAtom( atom ):
    
    print '   Atom:   '
    print 'atom name: ', atom.atomID
    print 'size:', atom.size
    print 'multiplicity: ', atom.multiplicity

    
def PrintTimesInTree( tree ):
    
    print '   TimesInTree:   '
    print 'count: ', tree.count
    print 'Elem:'
    
    elem = tree.atomicTree
    
    if isinstance( elem, Atom ):
        
        PrintAtom( elem )
        
    elif isinstance( elem, SumOfTrees ):
        
        PrintSumOfTrees( elem )
        
    elif isinstance( elem, TimesInTree ):
        
        PrintTimesInTree( elem )
  
        
def PrintSumOfTrees( tree ):
    
    leftSubTree = tree.atomicTreeL
    rightSubTree = tree.atomicTreeR
    print '   SumOfTrees:   '
    
    print '\nVar Left:'
    if isinstance( leftSubTree, Atom ):
        
        PrintAtom( leftSubTree )
        
    elif isinstance( leftSubTree, SumOfTrees ):
        
        PrintSumOfTrees( leftSubTree )
        
    elif isinstance( leftSubTree, TimesInTree ):
        
        PrintTimesInTree( leftSubTree )
        
    print '\nVar Right:'
    if isinstance( rightSubTree, Atom ):
        
        PrintAtom( rightSubTree )
        
    elif isinstance( rightSubTree, SumOfTrees ):
        
        PrintSumOfTrees( rightSubTree )
        
    elif isinstance( rightSubTree, TimesInTree ):
        
        PrintTimesInTree( rightSubTree )

        
def PrintTree( tree ):
    
    print '*** Tree characteristics: ***'
    print 'tree name: ', tree.treeName
    if isinstance( tree, Atom ):
        
        PrintAtom( tree )
        
    elif isinstance( tree, SumOfTrees ):
        
        PrintSumOfTrees( tree )
        
    elif isinstance( tree, TimesInTree ):
        
        PrintTimesInTree( tree )
    
    print '______________________\n'
    

def PrintTreeList( treeList ):
    
    print "\n TREES:\n"
    for tree in treeList:
        
        PrintTree( tree )
        
def PrintTreeDict( treeList ):
    
    print "\n TREES:\n"

    for tree in treeList.values():
        
        PrintTree( tree )