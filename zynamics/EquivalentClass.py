# -*- coding: utf-8 -*-

######### TERM VALUES #############

class EqClass:
    """
    commonEl: EqClass()         # representative(common) element of EqClass
    multiplicity: int           # multiplivity of this node in Main tree
    """
    commonEl = None 
    multiplicity = None
    
    def GetNewVar( self, newCommonEl ):
        """
        create new element, the only one in own equivalence class
        newCommonEl: EqClass()  # set new element, associated with equivalence class
        ret: EqClass()          # return EqClass with new common element
        """
        self.commonEl = newCommonEl  
        return self
        
        
class Atomic( EqClass ):

    def __init__( self, size, nodeName ):
        """
        size: int               # size of tree node
        nodeName: 'string'      # name of tree node
        """
        self.size = size
        self.nodeName = nodeName
        self.commonEl = self
        
class SumOfElems( EqClass ):
    
    def __init__( self, varL, varR ):
        """
        varL: EqClass()         # left part of element
        varR: EqClass()         # right part of element
        """
        self.varL = varL
        self.varR = varR
        self.commonEl = self
        
        
class TimesInArray( EqClass ):
    
    def __init__( self, count, var ):
        """
        count: int              # count same nodes
        elem: EqClass()         # element of array node
        """
        self.count = count
        self.elem = var
        self.commonEl = self