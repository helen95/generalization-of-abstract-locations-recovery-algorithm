# -*- coding: utf-8 -*-


class AtomicReference(): # AtomID \ count_elems
    """
    atomID: EqClass()           # element of EqClass from constraints (result of ASI)
    count_elems: int            # number of atomID elements in ASI-tree
    """
    def __init__( self, atomID, count_elems ):
        
        self.atomID = atomID
        self.count_elems = count_elems
