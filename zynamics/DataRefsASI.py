# -*- coding: utf-8 -*-

from math import floor
from library import gcd
from DataRefClass import *

from library import DEBUG

class StridedIntervals : 
        
    def __init__( self, low, up, step, regionName = "AR_Main" ) : # step[low, up]
        
        self.step = step   
        self.low = low + 1
        self.up = up + 1
        self.regionName = regionName # regionName -- String  

def IsSingletone( stridedInterval ) :  # straided interval

    return ( stridedInterval.step == 0 ) and \
           ( stridedInterval.low == stridedInterval.up )

def GetDataRef( newDataRef, memoryRegion, sz ):
    
    if newDataRef:
        return newDataRef.dataRef
    else:
        return ProgVar( memoryRegion, sz )
          
        
def ConvertSI2ASIrefs( memoryRegion, stridedInterval, length, dataRef = None ) : # length -- number of bytes of REIL-instruction
    
    if stridedInterval.low > 0x00: ## DO NOT NEED THIS
        
        stridedInterval.up = stridedInterval.up - stridedInterval.low + 1
        stridedInterval.low = 1	

    if stridedInterval.low < 0x00:
        
        stridedInterval.up = stridedInterval.up - stridedInterval.low + 1
        stridedInterval.low = 1

    if IsSingletone( stridedInterval ) :
        
        rightBound = stridedInterval.low + length - 1
        
        if DEBUG:
            print ( memoryRegion, '[' + str( stridedInterval.low ) + ':' + \
                                 str( rightBound ) + ']', True )
        return ( IntervalOfDataRef( GetDataRef( dataRef, memoryRegion, length ), \
                                   stridedInterval.low, rightBound ), True )
        
    else :
        
        size = max( stridedInterval.step, length )
        numOfElems = int( floor( ( stridedInterval.up - stridedInterval.low ) \
                                / size ) + 1)

        if DEBUG:
            referenceASI = ''.join( [  \
                 '[' , \
                  str(stridedInterval.low) , \
                  ':' , \
                  str(stridedInterval.up + size - 1) , \
                  ']\\' , \
                  str(numOfElems) , \
                  '[1:' , \
                  str(length) , \
                  ']' ] )
            print ( memoryRegion, referenceASI, ( stridedInterval.step >= length ) )
        
        memorySz = stridedInterval.up + size - stridedInterval.low
        return ( IntervalOfDataRef( ArrayOfDataRef( \
                IntervalOfDataRef( GetDataRef( dataRef, memoryRegion, memorySz ), \
                stridedInterval.low, stridedInterval.up + size - 1 ), \
                        numOfElems ), 1, length ), \
                        ( stridedInterval.step >= length ) )
 
#"""																
# SI - Strided Intervals
def SumSI( stridedInterval1, stridedInterval2 ) :
    
    lBound = stridedInterval1.low + stridedInterval2.low
    uBound = stridedInterval1.up + stridedInterval2.up
    u = stridedInterval1.low & stridedInterval2.low & ~lBound & \
        ~(stridedInterval1.up & stridedInterval2.up & uBound)
        
    v = ( ( stridedInterval1.low ^ stridedInterval2.low ) | \
        ~( stridedInterval1.low ^ lBound ) ) & ( ~stridedInterval1.up & \
        ~stridedInterval2.up & uBound )
    
    if ( u | v < 0x00 ) : # u < 0 ???????????????????
     
        step = 0x01
        lBound = 0x80000000
        uBound = 0x7FFFFFFF
    else :
        
        step = gcd( stridedInterval1.step, stridedInterval2.step )
        
    return StridedIntervals( lBound, uBound, step )
        
        
def ConvertTwoSIsToASIrefs( memoryRegion, stridedInterval1, stridedInterval2, length ) : # memReg = [base + index]
    
    if IsSingletone( stridedInterval1 ) or IsSingletone( stridedInterval2 ) :
        
        return ConvertSI2ASIrefs( memoryRegion, SumSI( stridedInterval1, \
                                                stridedInterval2 ), length )
        
    if stridedInterval1.step >= ( stridedInterval2.up - stridedInterval2.low + length ) :
        
        baseSI = stridedInterval1
        indexSI = stridedInterval2
        
    else:
        
        if stridedInterval2.step >= ( stridedInterval1.up - stridedInterval1.low + length ) :
            
            baseSI = stridedInterval2
            indexSI = stridedInterval1
            
        else :
            
            return ConvertSI2ASIrefs( memoryRegion, SumSI( stridedInterval1, \
                                                stridedInterval2 ), length )
            
    ( baseRef, _ ) = ConvertSI2ASIrefs( memoryRegion, baseSI, baseSI.step )
    ( refASI, isExactRef ) = ConvertSI2ASIrefs( "", indexSI, length, baseRef )
    return ( refASI, isExactRef )
    
    
   
def main() :
    
    si1 = StridedIntervals(-50, -10, 10)
    si2 = StridedIntervals(0, 9, 1)
    #res = ConvertTwoSIsToASIrefs("AR_Main", si1, si2, 1)
    si = StridedIntervals(-40, -8, 8)
    
    si = StridedIntervals(-44, -8, 4)
    si = StridedIntervals(4294967293, 4294967293, 4)
    res = ConvertSI2ASIrefs("mam", si, 4)
    print res#.dataRef.dataRef.dataRef.dataRef.dataRef.sz
    
				
    
if __name__ == '__main__':
    
	main()
#"""