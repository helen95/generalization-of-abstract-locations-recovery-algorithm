# -*- coding: utf-8 -*-

from PrintEqClass import PrintEqClass

def PrintAtomicRefList( atomicRefList ):
    
    for atomicRef in atomicRefList:
        
        resStr = ""
        resStr += PrintEqClass( atomicRef.atomID )
        countElStr = "count elements: " + str(atomicRef.count_elems) + "\n\n"
        resStr += countElStr
        print "count elements: ", atomicRef.count_elems, '\n\n'
        return resStr
								
def PrintAtomicRef( atomicRef ):
    
	PrintEqClass( atomicRef.atomID )
	print "count elements: ", atomicRef.count_elems, '\n\n'							
        