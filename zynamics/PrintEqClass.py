# -*- coding: utf-8 -*-

from EquivalentClass import Atomic
from EquivalentClass import SumOfElems
from EquivalentClass import TimesInArray

from AtomicTreeClass import DICT_OF_ATOM_TREES

def PrintAtomic( eqClass ):
    
    resStr = ""
    resStr += "___Atomic:___\n" + "size: " + str( eqClass.commonEl.size ) + \
				"\nnodeName: "  + str( eqClass.commonEl.nodeName ) + "\n"
    print '___Atomic:___'
    print 'size:', eqClass.commonEl.size
    print 'nodeName:', eqClass.commonEl.nodeName
    return resStr

    
def PrintTimesInArray( eqClass ):
    
    resStr = ""
    resStr += "___TimesInTree:___\n" + "count: " + str( eqClass.count ) + \
				"\n Elem: "
				
    print '___TimesInTree:___'
    print 'count: ', eqClass.count
    print 'Elem:'
    
    elem = eqClass.elem
    
    if isinstance( elem, Atomic ):
        
        resStr += PrintAtomic( elem )
        
    elif isinstance( elem, SumOfElems ):
        
        resStr += PrintSumOfElems( elem )
        
    elif isinstance( elem, TimesInArray ):
        
        resStr += PrintTimesInArray( elem )
    resStr += "\n"
    return resStr
  
        
def PrintSumOfElems( eqClass ):
    
    resStr = ""
    resStr += "___SumOfElems:___\n" + "\nVar Left: "
				
    leftSubTree = eqClass.varL
    rightSubTree = eqClass.varR
    print '___SumOfElems:___'
    
    print '\nVar Left:'
    if isinstance( leftSubTree, Atomic ):
        
        resStr += PrintAtomic( leftSubTree )
        
    elif isinstance( leftSubTree, SumOfElems ):
        
        resStr += PrintSumOfElems( leftSubTree )
        
    elif isinstance( leftSubTree, TimesInArray ):
        
        resStr += PrintTimesInArray( leftSubTree )
     
    resStr += "\n\nVar Right: "
				
    print '\nVar Right:'
    if isinstance( rightSubTree, Atomic ):
        
        resStr += PrintAtomic( rightSubTree )
        
    elif isinstance( rightSubTree, SumOfElems ):
        
        resStr += PrintSumOfElems( rightSubTree )
        
    elif isinstance( rightSubTree, TimesInArray ):
        
        resStr += PrintTimesInArray( rightSubTree )

    return resStr
        
def PrintEqClass( eqClass ):
    
    resStr = ""
    if isinstance( eqClass, Atomic ):
        
        resStr += PrintAtomic( eqClass )
        
    elif isinstance( eqClass, SumOfElems ):
        
        resStr += PrintSumOfElems( eqClass )
        
    elif isinstance( eqClass, TimesInArray ):
        
        resStr += PrintTimesInArray( eqClass )
    
    resStr += "******************\n"
    print '******************'
    return resStr
    

def PrintEqClassList( eqClassList ):
    
    for eqClass in eqClassList:
        
        PrintEqClass( eqClass )