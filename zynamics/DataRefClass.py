# -*- coding: utf-8 -*-


####### DATA REFERENCES ###########


class DataRef:                      # Data Reference X[ 1:20 ]\10
    pass

class ProgVar( DataRef ):           # X -- name of variable in DataRef
    
    def __init__( self, varName, sz ):
        """
        varName: 'string'           # variable name
        sz: int                     # size of variable
        """
        self.varName = varName
        self.sz = sz                
        
        
class IntervalOfDataRef( DataRef ): # X[ 1:20 ]
    
    def __init__( self, dataRef, low, up ):
        """
        dataRef: DataRef()          # part of DataRef, that include this interval 
        low: int                    # left bount of interval
        up: int                     # right bound of interval
        """
        self.dataRef = dataRef      
        self.low = low              
        self.up = up                
        
        
class ArrayOfDataRef( DataRef ):    # X[ 1:20 ]\10
    
    def __init__( self, dataRef, countOfElem ):
        """
        dataRef: DataRef()          # part of DataRef, that include this array 
        countOfElem: int            # count of element in array
        """
        self.dataRef = dataRef
        self.countOfElem = countOfElem
