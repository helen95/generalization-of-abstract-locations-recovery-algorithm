# -*- coding: utf-8 -*-

from library import DEBUG

from ConstraintsClass import EQUIVALENT
from AtomicTreeClass import DICT_OF_ATOM_TREES

from library import lcm

from EquivalentClass import EqClass
from EquivalentClass import Atomic
from EquivalentClass import SumOfElems
from EquivalentClass import TimesInArray

from AtomicRefsClass import AtomicReference


############################ Global definitions ###############################

"""
LIST_OF_ATOM_ID: list( 'string' )
"""
LIST_OF_ATOM_ID = [] # list of used variable names


################################ FUNCTIONS ####################################      

    
        

def Union( class1, class2, commonEl ):#, name1, name2 ):
    """
    # unite 2 equivalece classes class1 and class2 in one by their associated element commonEl
    class1: EqClass()
    class2: EqClass()
    commonEl: EqClass()
    """   
    SetValToEqClass( class1, commonEl )
    SetValToEqClass( class2, commonEl )
    #class1.commonEl = commonEl
    #class2.commonEl = commonEl
    #DICT_OF_ATOM_TREES[ name1 ] = class1
    #DICT_OF_ATOM_TREES[ name2 ] = class2
        
        
  
def IsEquivalent( class1, class2 ):
    """
    # check if equivalence classes class1 and class2 are eqivalent   
    class1: EqClass()
    class2: EqClass()
    ret: bool
    """
    return class1.commonEl == class2.commonEl

    
def FindValOfEqClass( eqClass ):
    """
    eqClass: EqClass()          # equivalence class
    ret: EqClass()              # return common element of eqClass
    """
    return eqClass.commonEl

    
def SetValToEqClass( eqClass, newCommonEl ):
    """
    eqClass: EqClass()          
    newCommonEl: EqClass()      # new common element for eqClass
    ret: EqClass()              # reurn new eq. class
    """
    eqClass.commonEl = newCommonEl
    return eqClass

   
def Abs( x ): 
    """
    # length of variable x
    x: EqClass()                # node
    ret: int                    # seturn size of node
    """
    elem = FindValOfEqClass(x) # x.commonEl
    
    if ( isinstance( elem, Atomic ) ) :
        
        return elem.size
        
    else:
        if( isinstance( elem, SumOfElems ) ) :
            
            return Abs( elem.varL ) + Abs( elem.varR )
            
        else:
            if( isinstance( elem, TimesInArray ) ) :
                
                return elem.count * Abs( elem.elem )
        

def Sqr( v ):
    """
    v: EqClass()                # node of tree
    ret: EqClass()              
    """
    if ( isinstance( v, TimesInArray ) ) and ( 0x01 == v.count ):
        
        return v.elem
        
    else :
        
        return v.GetNewVar(v)

        
def Split( x, n ):
    """
    x: EqClass()                # node of tree 
    n: int                      # index of breakpoint
    ret: EqClass()              # return splited new eq. class with breakpoin in point n 
    """
    if ( 0 < n ) and ( n < Abs( x ) ) :
        
        xValue = FindValOfEqClass( x )
        
        if ( isinstance( xValue, Atomic ) ) :
            
            varL = Atomic( n, xValue.nodeName )
            varR = Atomic( xValue.size - n, xValue.nodeName )
            x = SetValToEqClass( x, SumOfElems( Sqr( varL ), Sqr( varR ) ) )
            return x
            
        elif ( isinstance( xValue, SumOfElems ) ) :
            
            if n < Abs( xValue.varL ) :
                
                return Split( xValue.varL, n )
                
            else :
                
                return Split( xValue.varR, n - Abs( xValue.varL ) )
                
        elif ( isinstance( xValue, TimesInArray ) ) :
            
            p = max( 1, n / Abs( xValue.elem ) )
            varL = Sqr( TimesInArray( p, xValue.elem ) )
            varR = Sqr( TimesInArray( xValue.count - p, xValue.elem ) )
            x = SetValToEqClass( x, SumOfElems( varL, varR ) ) 
            
            return Split( xValue, n )

            
def Refine( x, n ):
    """
    x: EqClass()                # node 
    n: int                      # index of breakpoint
    ret list( EqClass() )       # return list of node childs
    """
    leaves = []
    xValue = FindValOfEqClass( x )
    
    if ( isinstance( xValue, SumOfElems ) ) :
        
        leaves += [ xValue.varL, xValue.varR ]
        
    elif ( isinstance( xValue, Atomic ) ) :
        
        x = Split( xValue, n )        ########## Значение x поменялось???
        leaves += Refine( x, n )
              
    elif ( isinstance( xValue, TimesInArray ) ) :
        
        p = max( 1, n / Abs( xValue.elem ) ) # point, which split array on 2 part
        varL = Sqr( TimesInArray( p, xValue.elem ) )
        varR = Sqr( TimesInArray( xValue.count - p, xValue.elem ) )
        x = SetValToEqClass( x, SumOfElems( varL, varR ) ) 
        leaves += Refine( x, n )
        
    return leaves

        


###################### Unification Algorithm ##################################


def UnifyAtom( x ):
    """
    x: EqClass()                # atomic node
    ret: EqClass()              # return common element of atomic node
    """
    return FindValOfEqClass( x )

    
def UnifyList( var1, var2 ):
    """
    var1: list( EqClass() )     # list of nides
    var2: list( EqClass() )     # list of nodes
    ret: EqClass()              # return eq.class corresponding var1 and var2
    """
    x1 = var1[0]
    r1 = var1[1:]
    x2 = var2[0]
    r2 = var2[1:]
    
    if Abs( x1 ) == Abs( x2 ):
        
        if ( r1, r2 ) == ( [], [] ):
            
            return Unify( x1, x2 )
            
        else:
            
            var1 = Sqr( Unify( x1, x2 ) )
            var2 = Sqr( UnifyList( r1, r2 ) )
            return SumOfElems( var1, var2 )
            
    elif Abs( x1 ) > Abs( x2 ):
        
        return UnifyList( Refine( x1, Abs( x2 ) ) + r1, [ x2 ] + r2 )
    
    else: #Abs( x1 ) < Abs( x2 )
        
        return UnifyList( [ x1 ] + r1, Refine( x2, Abs( x1 ) ) + r2 )
    

def Exp( t, i ):
    """
    t: EqClass()                # node of tree
    i: int                      # count of this nodes in tree
    ret: EqClass()              # present array as sum of count nodes
    """
    if 0x01 == i:
        return t
    else:
        return Sqr( SumOfElems( t, Exp( t, i - 1 ) ) )
   
        
def UnifyArrays( arr1, arr2 ):
    """
    arr1: TimesInArray()        # array of nodes 
    arr2: TimesInArray()        # array of nodes
    ret: TimesInArray()         # return new unify array
    """
    num1 = arr1.count 
    elem1 = arr1.elem
    num2 = arr2.count
    elem2 = arr2.elem
    
    m = lcm( Abs( elem1 ), Abs( elem2 ) ) / Abs( elem1 )
    x1 = Exp( elem1, m )                                        # subarray1
    x2 = Exp( elem2, num2 * m / num1 ) #Exp( elem2, m ) ???     # subarray2
    unifyElem = Unify( x1, x2 )
    z = TimesInArray( num1 * Abs( elem1 ) / Abs( x1 ), unifyElem )
    return z

    
def Unify( x, y ):
    """
    x: EqClass()                # node
    y: EqClass()                # node
    ret: EqClass()              # return unified node
    """
    if not IsEquivalent( x, y ):
        
        x_value = FindValOfEqClass( x )
        y_value = FindValOfEqClass( y )
        
        if isinstance( x_value, Atomic ): ### Supposed to be just X ???
        
            joinElem = UnifyAtom( y )
            
        elif isinstance( y_value, Atomic ):
            
            joinElem = UnifyAtom( x )
            
        elif isinstance( x_value, SumOfElems ):
            
            joinElem = UnifyList( [ x_value.varL, x_value.varR ], [ y ] )
            
        elif isinstance( y_value, SumOfElems ):
            
            joinElem = UnifyList( [ x ], [ y_value.varL, y_value.varR ] )
            
        elif ( isinstance( x_value, TimesInArray ) ) and \
                                    ( isinstance( y_value, TimesInArray ) ):
            
            joinElem = UnifyArrays( x_value, y_value )
        
        Union( x, y, joinElem )
    
    return FindValOfEqClass( x )

  
#################################### MAIN SECTION ############################

def ProcessConstraint( x, constraint, y ):
    """
    x: list( EqClass() )                # left part of constraint (list of nodes)
    constraint: EQUIVALENT or LARGER
    y: list( EqClass() )                # right part of constraint (list of nodes)
    ret: EqClass()                      # return unified node
    """
    if constraint == EQUIVALENT:
        
        return UnifyList( x, y )
    

        
def SolveConstraint( terms ):
    """
    terms: Term( AtomicReference() )    # left and right lists of constraints (and operation)
    ret: list( EqClass() )              # return list of used leaves 
    """
    type_res = []
    
    left = [ i.atomID for i in terms.leftDataRef ]
    right = [ i.atomID for i in terms.rightDataRef ]      
    type_res = ProcessConstraint( left, terms.constraint, right )
    
    return type_res

      
